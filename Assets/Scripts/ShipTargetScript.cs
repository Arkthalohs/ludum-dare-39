﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ShipTargetScript : MonoBehaviour
{
    public Transform EnemySectorHolder;
    public Transform Selector;
    public LineRenderer Line;
    public LineRenderer Phaser;

    private Vector3 _weaponryStart;
    private int _shotShips;
    private bool _shootAnim;

    private void Start()
    {
        _weaponryStart = FindObjectsOfType<ShipRoomBehaviour>().First(x => x.Room == ShipRoomDefinitions.Weapons).transform.position;
        _weaponryStart = new Vector2(_weaponryStart.x, _weaponryStart.y);
    }

    public IEnumerator WaitForTargetSelection(int thingsToShoot)
    {
        gameObject.SetActive(true);
        Selector.GetComponent<Image>().enabled = false;
        Setup();
        _shotShips = 0;
        while (_shotShips < thingsToShoot && EnemySectorHolder.GetComponentsInChildren<EnemyShipBehaviour>().Where(x => x.GetComponentInChildren<Image>().enabled).Count() > 0)
        {
            yield return null;
        }

        foreach (var enemy in EnemySectorHolder.GetComponentsInChildren<EnemyShipBehaviour>())
        {
            enemy.Pause(false);
        }

        Line.SetPositions(new Vector3[0]);
        Line.gameObject.SetActive(false);
        Phaser.gameObject.SetActive(false);
        gameObject.SetActive(false);
    }

    private void Update()
    {
        if (_shootAnim)
        {
            return;
        }

        var sector = Sector.None;
        var mouse = Input.mousePosition;
        mouse -= Vector3.up * Screen.height / 2 + Vector3.right * Screen.width / 2;
        if (mouse.x > 0 && mouse.y > 0)
        {
            sector = Sector.NorthEast;
        }
        else if (mouse.x > 0 && mouse.y < 0)
        {
            sector = Sector.SouthEast;
        }
        else if (mouse.x < 0 && mouse.y > 0)
        {
            sector = Sector.NorthWest;
        }
        else if (mouse.x < 0 && mouse.y < 0)
        {
            sector = Sector.SouthWest;
        }

        Selector.GetComponent<Image>().enabled = false;
        if (sector != Sector.None && EnemySectorHolder.GetChild((int)sector - 1).childCount > 1)
        {
            Selector.GetComponent<Image>().enabled = true;
            Line.gameObject.SetActive(true);
            Selector.position = EnemySectorHolder.GetChild((int)sector - 1).GetComponentsInChildren<EnemyShipBehaviour>()[0].transform.position;
            var pos = new Vector3(Selector.position.x, Selector.position.y);
            pos -= (_weaponryStart - pos).normalized * -.4f;
            Line.SetPositions(new Vector3[] { _weaponryStart, pos });
        }
        else
        {
            Line.gameObject.SetActive(false);
        }


        if (Input.GetMouseButtonUp(0) && Selector.GetComponent<Image>().enabled)
        {
            Line.gameObject.SetActive(false);
            StartCoroutine(Attack(EnemySectorHolder.GetChild((int)sector - 1).GetComponentsInChildren<EnemyShipBehaviour>()[0].gameObject));
        }
    }

    private IEnumerator Attack(GameObject ship)
    {
        _shootAnim = true;
        foreach (var enemy in EnemySectorHolder.GetComponentsInChildren<EnemyShipBehaviour>())
        {
            enemy.Pause(false);
        }

        Selector.gameObject.SetActive(false);
        var pos = new Vector3(_weaponryStart.x, _weaponryStart.y, ship.transform.position.z);
        Phaser.SetPosition(1, pos);
        Phaser.SetPosition(0, pos);
        for (var i = 0; i < 3; i++)
        {
            var frames = 30f;
            var _fireOffset = UnityEngine.Random.onUnitSphere * (i - 2);

            yield return new WaitForSeconds(.1f);
            Phaser.gameObject.SetActive(true);
            SoundManager.Instance.PlaySound("shoot", 6);
            for (var j = frames; j >= 0; j--)
            {
                Phaser.gameObject.SetActive(true);
                Phaser.SetPosition(1, ship.transform.position + _fireOffset);
                var alpha = j / frames;
                Phaser.endColor = new Color(Phaser.endColor.r, Phaser.endColor.g, Phaser.endColor.b, alpha);
                Phaser.startColor = new Color(Phaser.startColor.r, Phaser.startColor.g, Phaser.startColor.b, alpha);
                yield return null;

                if (i == 2 && j < 30 && ship.GetComponentInChildren<Image>().enabled)
                {
                    StartCoroutine(PlayParticle(ship));
                }
            }
        }

        _shotShips++;

        foreach (var enemy in EnemySectorHolder.GetComponentsInChildren<EnemyShipBehaviour>())
        {
            enemy.Pause(true);
        }

        Phaser.gameObject.SetActive(false);

        _shootAnim = false;
    }

    private IEnumerator PlayParticle(GameObject ship)
    {
        SoundManager.Instance.PlaySound("explosion", 6);
        ship.GetComponentInChildren<Image>().enabled = false;
        ship.transform.SetParent(ship.transform.parent.parent, true);
        foreach (var p in ship.GetComponentsInChildren<ParticleSystem>(true))
        {
            p.gameObject.SetActive(true);
            p.Play();
        }

        while (ship.GetComponentInChildren<ParticleSystem>().IsAlive())
        {
            yield return null;
        }

        Destroy(ship);
    }

    private void Setup()
    {
        foreach (var enemyB in EnemySectorHolder.GetComponentsInChildren<EnemyShipBehaviour>())
        {
            var enemy = enemyB;
            enemy.Pause(true);
            ////var trigger = enemy.gameObject.AddComponent<EventTrigger>();

            ////var onEnter = new EventTrigger.Entry();
            ////onEnter.eventID = EventTriggerType.PointerEnter;
            ////onEnter.callback.AddListener(new UnityEngine.Events.UnityAction<BaseEventData>(new UnityEngine.Events.UnityAction<BaseEventData>(a => Enter(enemy))));
            ////trigger.triggers.Add(onEnter);


            ////var onExit = new EventTrigger.Entry();
            ////onExit.eventID = EventTriggerType.PointerEnter;
            ////onExit.callback.AddListener(new UnityEngine.Events.UnityAction<BaseEventData>(new UnityEngine.Events.UnityAction<BaseEventData>(a => Exit(enemy))));
            ////trigger.triggers.Add(onExit);
        }
    }
}
