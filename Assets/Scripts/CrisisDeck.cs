﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class CrisisDeck
{
    private List<CrisisCard> _cards;
    private List<CrisisCard> _discards;

    public CrisisDeck(Func<List<CrisisCard>> populateDeck)
    {
        _cards = new List<CrisisCard>();
        _discards = new List<CrisisCard>();
        if (populateDeck != null)
        {
            _cards = populateDeck();
        }
        else
        {
            DefaultPopulate();
        }

        Shuffle();
    }

    public void DefaultPopulate()
    {
        AddCount(CrisisDefinitions.DoubleTrouble, 1);
        AddCount(CrisisDefinitions.DamageRoom(ShipRoomDefinitions.Bridge), 2);
        AddCount(CrisisDefinitions.DamageRoom(ShipRoomDefinitions.Shields), 1);
        AddCount(CrisisDefinitions.DamageRoom(ShipRoomDefinitions.Weapons), 2);
        AddCount(CrisisDefinitions.DamageRoom(ShipRoomDefinitions.Holodeck), 1);
        AddCount(CrisisDefinitions.DamageRoom(ShipRoomDefinitions.Sensors), 3);
        AddCount(CrisisDefinitions.DamageRoom(ShipRoomDefinitions.Medbay), 1);
        AddCount(CrisisDefinitions.DamageRoom(ShipRoomDefinitions.Engineering), 4);

        AddCount(CrisisDefinitions.MutinyRoom(ShipRoomDefinitions.Bridge), 1);
        AddCount(CrisisDefinitions.MutinyRoom(ShipRoomDefinitions.Shields), 1);
        AddCount(CrisisDefinitions.MutinyRoom(ShipRoomDefinitions.Weapons), 1);
        AddCount(CrisisDefinitions.MutinyRoom(ShipRoomDefinitions.Holodeck), 4);
        AddCount(CrisisDefinitions.MutinyRoom(ShipRoomDefinitions.Sensors), 2);
        AddCount(CrisisDefinitions.MutinyRoom(ShipRoomDefinitions.Medbay), 4);
        AddCount(CrisisDefinitions.MutinyRoom(ShipRoomDefinitions.Engineering), 1);

        AddCount(CrisisDefinitions.FtlDischarge, 3);
        AddCount(CrisisDefinitions.InTheVents, 2);
        AddCount(CrisisDefinitions.Nothing, 5);
    }

    public CrisisCard DrawCard()
    {
        if (_cards.Count == 0)
        {
            Shuffle();
        }

        var card = _cards[0];
        _discards.Add(card);
        _cards.RemoveAt(0);
        return card;
    }

    private void AddCount(CrisisCard card, int count)
    {
        for (var i = 0; i < count; i++)
        {
            _cards.Add(card);
        }
    }

    private void Shuffle()
    {
        var newDeck = new List<CrisisCard>(_cards.Count);
        var random = new UnityEngine.Random();
        while (_cards.Count > 0)
        {
            var idx = UnityEngine.Random.Range(0, _cards.Count);
            newDeck.Add(_cards[idx]);
            _cards.RemoveAt(idx);
        }

        while (_discards.Count > 0)
        {
            var idx = UnityEngine.Random.Range(0, _discards.Count);
            newDeck.Add(_discards[idx]);
            _discards.RemoveAt(idx);
        }

        _cards = newDeck;
    }
}
