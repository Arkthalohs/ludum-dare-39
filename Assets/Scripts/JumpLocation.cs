﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class JumpLocation
{
    public string Name;
    public string Description;
    public string PictureName;
    public int Distance;
    public Action<ControllerBehaviour, ShipViewModel> OnJumpComplete;
    public CrisisDeck CrisisDeck;

    public JumpLocation(string name, string description, string pictureName, int distance, Action<ControllerBehaviour, ShipViewModel> onComplete, CrisisDeck deck)
    {
        Name = name;
        Description = description;
        PictureName = pictureName;
        Distance = distance;
        OnJumpComplete = onComplete;
        CrisisDeck = deck;
    }
}
