﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ShipViewModel
{
    public ShipModel Ship;

    public ShipRoomBehaviour _bridge;

    public ShipViewModel()
    {
        Ship = new ShipModel();
        ShipRoomDefinitions.Engineering.MinCharge = Ship.MinJumpCharge;
        ShipRoomDefinitions.Engineering.MaxCharge = Ship.MaxJumpCharge;
    }

    public void UseRoom(ControllerBehaviour player, CharacterViewModel character, ShipRoom room)
    {
        if (Ship.EnergyRemaining >= room.EnergyCost)
        {
            Ship.EnergyRemaining -= room.EnergyCost;
            room.OnUse(player, this, character);
        }
    }

    public void ChargeJumpDrive(int amount = 1)
    {
        Ship.JumpCharge = Mathf.Clamp(Ship.JumpCharge + amount, 0, Ship.MaxJumpCharge);
        ShipRoomDefinitions.Engineering.CurrentCharge = Ship.JumpCharge;
    }

    public void SetJumpLocation(JumpLocation jumpLocation)
    {
        Ship.NextJumpLocation = jumpLocation;
    }

    public void SetJumpLocation(JumpLocationBehaviour jumpLocation)
    {
        Ship.NextJumpLocation = jumpLocation.Contents;
        _bridge.SetHeldCard(jumpLocation.transform);
    }

    public void ExecuteJump(ControllerBehaviour player, int pilotingSuccesses)
    {
        if (Ship.CanJump())
        {
            if (Ship.RiskyJump() && pilotingSuccesses < (int)Mathf.FloorToInt(Ship.NextJumpLocation.Distance / 10))
            {
                DamageHull(2);
            }

            Ship.DistanceToEarth -= Ship.NextJumpLocation.Distance;
            Ship.NextJumpLocation.OnJumpComplete(player, this);
            Ship.NextJumpLocation = null;
            Ship.JumpCharge = 0;
            ShipRoomDefinitions.Engineering.CurrentCharge = 0;
        }
    }

    public void DamageShields(Sector sector, int amt)
    {
        Ship.Shields[sector] = Mathf.Max(Ship.Shields[sector] - amt, 0);
    }

    public void DamageRoom(ShipRoom room, int damageAmount, Sector damageSourceDir = Sector.None)
    {
        var shieldDamage = Mathf.Min(Ship.Shields[damageSourceDir], damageAmount);
        if (shieldDamage > 0)
        {
            Ship.Shields[damageSourceDir] -= shieldDamage;
            damageAmount -= shieldDamage;
        }

        var hullDamage = Mathf.Max(0, room.CurrentDamage + damageAmount - room.CrewCapacity);
        var roomDamage = damageAmount - hullDamage;
        Debug.Log(room.Name + " " + hullDamage + " " + roomDamage + " " + damageSourceDir);
        if (hullDamage > 0)
        {
            DamageHull(hullDamage, damageSourceDir);
        }

        if (roomDamage > 0)
        {
            room.Damage(roomDamage);
            foreach (var character in room.PresentCrew)
            {
                character.Damage(roomDamage);
            }
        }
    }

    public void DamageHull(int damageAmount, Sector damageSourceDir = Sector.None)
    {
        var shieldDamage = Mathf.Min(Ship.Shields[damageSourceDir], damageAmount);
        if (shieldDamage > 0)
        {
            Ship.Shields[damageSourceDir] -= shieldDamage;
            damageAmount -= shieldDamage;
        }

        Ship.Hull -= damageAmount;
    }

    public void ConstructShip(ControllerBehaviour player, ShipRoomBehaviour roomPrefab, Transform shipUIParent)
    {
        _bridge = SpawnRoom(ShipRoomDefinitions.Bridge, new Vector3(190, 0), roomPrefab, shipUIParent, player);
        SpawnRoom(ShipRoomDefinitions.Holodeck, new Vector3(0, 0), roomPrefab, shipUIParent, player);
        SpawnRoom(ShipRoomDefinitions.Engineering, new Vector3(-190, 0), roomPrefab, shipUIParent, player);
        SpawnRoom(ShipRoomDefinitions.Sensors, new Vector3(-100, 70), roomPrefab, shipUIParent, player);
        SpawnRoom(ShipRoomDefinitions.Medbay, new Vector3(-100, -70), roomPrefab, shipUIParent, player);
        SpawnRoom(ShipRoomDefinitions.Shields, new Vector3(100, -70), roomPrefab, shipUIParent, player);
        SpawnRoom(ShipRoomDefinitions.Weapons, new Vector3(100, 70), roomPrefab, shipUIParent, player);
    }

    private ShipRoomBehaviour SpawnRoom(ShipRoom room, Vector3 location, ShipRoomBehaviour roomPrefab, Transform shipUIParent, ControllerBehaviour player)
    {
        var roomView = GameObject.Instantiate(roomPrefab, shipUIParent);
        roomView.transform.localPosition = location;
        roomView.Initialize(player, room);

        return roomView;
    }
}

