﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageFlashBehaviour : MonoBehaviour
{
    private Image Image;
    private float _lastChange;
    private bool _red;

    private void Start()
    {
        Image = GetComponent<Image>();
        _lastChange = Time.time + UnityEngine.Random.Range(0, .5f);
    }

    private void Update()
    {
        if (_lastChange + .5f < Time.time)
        {
            _lastChange = Time.time;
            Image.color = _red ? Color.white : Color.red;
            _red = !_red;
        }
    }
}
