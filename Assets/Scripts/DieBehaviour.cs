﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DieBehaviour : MonoBehaviour
{

    private Rigidbody _rigidBody;

    public bool IsTilted
    {
        get
        {
            return Stopped && Value == -1;
        }
    }

    public bool Stopped
    {
        get
        {
            return _rigidBody.IsSleeping();
        }
    }

    public int Value
    {
        get
        {
            var dotForward = Vector3.Dot(transform.forward, Vector3.up);
            var dotRight = Vector3.Dot(transform.right, Vector3.up);
            var dotUp = Vector3.Dot(transform.up, Vector3.up);
            if (dotForward > .99f)
            {
                return 6;
            }

            if (dotForward < -.99f)
            {
                return 1;
            }

            if (dotRight > .99f)
            {
                return 4;
            }

            if (dotRight < -.99f)
            {
                return 3;
            }

            if (dotUp > .99f)
            {
                return 5;
            }

            if (dotUp < -.99f)
            {
                return 2;
            }

            return -1;
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        SoundManager.Instance.PlaySound("die" + UnityEngine.Random.Range(1, 4), 5 * Mathf.Min(_rigidBody.velocity.magnitude, 10) / 10f);
    }

    public void Roll()
    {
        if (_rigidBody == null)
        {
            _rigidBody = GetComponent<Rigidbody>();
        }

        transform.localPosition = Vector3.zero + 1f * Vector3.up;
        transform.rotation = Random.rotationUniform;
        _rigidBody.AddExplosionForce(200, transform.position, 1);
        var dirForce = Random.insideUnitCircle.normalized;
        dirForce.y = .2f;
        _rigidBody.AddForce(dirForce * 300);
        _rigidBody.AddTorque(Random.onUnitSphere * 1000);
    }

    public void Untilt()
    {
        _rigidBody.AddForce(Random.onUnitSphere * 300);
        _rigidBody.AddTorque(Random.onUnitSphere * 1000);
    }

    private void Start()
    {
        if (_rigidBody == null)
        {
            _rigidBody = GetComponent<Rigidbody>();
        }
    }

    private void Update()
    {
        if (IsTilted)
        {
            Untilt();
        }
    }
}
