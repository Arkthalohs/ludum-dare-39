﻿public enum Sector
{
    None,
    NorthEast,
    SouthEast,
    SouthWest,
    NorthWest
}