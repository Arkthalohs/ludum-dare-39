﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CharacterPanelBehaviour : MonoBehaviour
{
    public GameObject CharacterPrefab;

    private List<CharacterViewModel> _characters;
    private List<CharacterPane> _characterPanes;
    private ControllerBehaviour _player;

    public void Initialize(ControllerBehaviour player)
    {
        _characters = player.Characters;
        _player = player;
        _characterPanes = new List<CharacterPane>();
        for (var i = 0; i < _characters.Count; i++)
        {
            AddPanel(_characters[i]);
        }
    }

    private void AddPanel(CharacterViewModel character)
    {
        var pane = Instantiate(CharacterPrefab, transform);
        var texts = pane.GetComponentsInChildren<Text>();
        pane.transform.GetChild(0).GetComponent<Image>().color = character.GetColor();
        _characterPanes.Add(new CharacterPane(
            character,
            pane,
            texts[0],
            texts[1],
            texts[2],
            texts[3],
            pane.transform.GetChild(3)
        ));

        pane.transform.GetChild(2).GetComponent<Text>().text = character.Name;
        pane.GetComponentInChildren<Button>().onClick.AddListener(new UnityEngine.Events.UnityAction(() => _player.SelectCharacter(character)));
    }

    private void Update()
    {
        foreach(var character in _characters)
        {
            if (!_characterPanes.Any(x => x.Character == character))
            {
                AddPanel(character);
            }
        }

        for (var i = 0; i < _characterPanes.Count; i++)
        {
            var pane = _characterPanes[i];
            pane.Root.GetComponentInChildren<Button>().interactable = pane.Character.CanTakeAction();
            if (pane.Character.Health == 0)
            {
                _characterPanes.RemoveAt(i--);
                Destroy(pane.Root);
            }

            for (var j = 0; j < pane.Health.Length; j++)
            {
                pane.Health[j].color = j < pane.Character.Health ? Color.red : Color.black;
            }

            pane.PilotText.text = string.Empty + pane.Character.PilotSkill;
            pane.CombatText.text = string.Empty + pane.Character.CombatSkill;
            pane.RepairText.text = string.Empty + pane.Character.RepairSkill;
            pane.SocialText.text = string.Empty + pane.Character.SocialSkill;
        }
    }

    private class CharacterPane
    {
        public GameObject Root;
        public Text PilotText;
        public Text CombatText;
        public Text RepairText;
        public Text SocialText;
        public Image[] Health;
        public CharacterViewModel Character;

        public CharacterPane(CharacterViewModel character, GameObject root, Text pilot, Text combat, Text repaire, Text social, Transform healthContainer)
        {
            Character = character;
            Root = root;
            PilotText = pilot;
            CombatText = combat;
            RepairText = repaire;
            SocialText = social;
            Health = new Image[healthContainer.childCount];
            for (var i = 0; i < healthContainer.childCount; i++)
            {
                Health[i] = healthContainer.GetChild(i).GetComponent<Image>();
            }
        }
    }
}
