﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {
    public List<Sprite> TutorialImages;
    public Image TutorialPane;

    private int _curTutorial;


    public void StartGame()
    {
        SceneManager.LoadScene("scene");
    }

    public void DoTutorial()
    {
        TutorialPane.gameObject.SetActive(true);
        _curTutorial = 0;
        TutorialPane.sprite = TutorialImages[_curTutorial];
        TutorialPane.GetComponentInChildren<Button>().onClick.RemoveAllListeners();
        TutorialPane.GetComponentInChildren<Button>().onClick.AddListener(new UnityEngine.Events.UnityAction(() => Next()));

    }

    private void Next()
    {
        _curTutorial++;
        if (_curTutorial >= TutorialImages.Count)
        {
            TutorialPane.gameObject.SetActive(false);
            return;
        }

        TutorialPane.sprite = TutorialImages[_curTutorial];
    }

    public void Quit()
    {
        Application.Quit();
    }
}
