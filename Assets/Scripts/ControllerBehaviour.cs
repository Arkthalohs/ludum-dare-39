﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControllerBehaviour : MonoBehaviour
{
    public Text PowerRemainingText;
    public Text DistanceToEarthText;
    public Text CurrentSectorText;
    public Transform ShipUIParent;
    public ShipRoomBehaviour RoomPrefab;
    public JumpLocationSelectorBehaviour JumpLocationSelector;
    public ActionChooserBehaviour ActionSelector;
    public ShipTargetScript ShipSelector;
    public JumpLocationBehaviour JumpLocationPrefab;
    public MeepleBehaviour MeeplePrefab;
    public Text DetailText;
    public Transform HullPanel;
    public CrisisCardPanel CrisisPanel;
    public Transform[] EnemyShipSectors;
    public Transform[] ShieldSectors;
    public EnemyShipBehaviour EnemyShipPrefab;
    public GraphicRaycaster Raycaster;

    public GameObject Defeat;
    public GameObject Victory;

    public Transform WarpLines;
    public DieRoller DieRoller;
    public HolodeckBehaviour Holodeck;

    public MeepleBehaviour SelectedMeeple;

    private ShipViewModel _ship;
    private List<MeepleBehaviour> _crew;
    private bool _processingTurnChange;
    private int _currentHullHealth;
    private CrisisDeck _crisisDeck;
    private JumpLocationDeck _jumpDeck;
    private Dictionary<Sector, IEnumerator> _shieldColorChange;
    private bool _coroutine;
    public List<CharacterViewModel> Characters;
    private List<ShipRoom> _roomsOnFire;
    private List<ShipRoom> _siegedRooms;

    public void SelectMeeple(MeepleBehaviour selected)
    {
        if (!selected.ViewModel.CanTakeAction())
        {
            return;
        }

        if (SelectedMeeple != null)
        {
            SelectedMeeple.SetSelected(false);
        }

        SelectedMeeple = selected;
        SelectedMeeple.SetSelected(true);
        SoundManager.Instance.PlaySound("blip", 6);

        foreach (var room in ShipUIParent.GetComponentsInChildren<ShipRoomBehaviour>())
        {
            room.GetComponent<Button>().interactable = room.Room.CanTakeAction(_ship, SelectedMeeple.ViewModel);
            room.PowerCost.color = _ship.Ship.EnergyRemaining < room.Room.EnergyCost ? Color.red : Color.black;
        }
    }

    public void BeginAwayMission(CharacterViewModel character)
    {
        StartCoroutine(AwayMissionCoroutine(character));
    }

    public void SelectCharacter(CharacterViewModel selected)
    {
        SelectMeeple(FindObjectsOfType<MeepleBehaviour>().First(x => x.ViewModel == selected));
    }

    public void StartFire(ShipRoom room)
    {
        _roomsOnFire.Add(room);
        room.IsOnFire = true;
        _ship.DamageRoom(room, 1);
    }

    public void StartMutiny(ShipRoom room)
    {
        room.InRevolt = true;
    }

    public void StartInvasion(ShipRoom room)
    {
        _siegedRooms.Add(room);
        room.Sieged = true;
    }

    public void StartAttack(CharacterViewModel character)
    {
        StartCoroutine(PerformAttackCoroutine(character));
    }

    public void MoveMeepleAndPerformAction(ShipRoomBehaviour newRoom)
    {
        if (SelectedMeeple && 
            SelectedMeeple.ViewModel.CanTakeAction() &&
            (SelectedMeeple.ViewModel.IsInRoom(newRoom) || newRoom.Room.HasCrewCapacity) &&
            newRoom.Room.CanTakeAction(_ship, SelectedMeeple.ViewModel))
        {
            newRoom.AddCrew(SelectedMeeple);
            StartCoroutine(ActInRoomCoroutine(newRoom));
            SoundManager.Instance.PlaySound("blip2", 6);
        }
    }

    public void ClearEnemyShips()
    {
        foreach (var enemy in EnemyShipSectors.SelectMany(x => x.GetComponentsInChildren<EnemyShipBehaviour>()))
        {
            Destroy(enemy.gameObject);
        }
    }

    public void SpawnEnemyInSector(int sectorNum, int numToSpawn)
    {
        var sector = EnemyShipSectors[sectorNum - 1];
        for (var i = 0; i < numToSpawn; i++)
        {
            Instantiate(EnemyShipPrefab, sector).transform.localPosition = Vector3.zero;
        }
    }

    public void SetCrisisDeck(CrisisDeck deckForCurrentLocation)
    {
        if (deckForCurrentLocation != null)
        {
            _crisisDeck = deckForCurrentLocation;
        }
    }

    public void Win()
    {
        Victory.SetActive(true);
        Victory.GetComponentInChildren<Button>().onClick.AddListener(new UnityEngine.Events.UnityAction(() => { SceneManager.LoadScene("Main Menu"); }));
    }

    public void Lose()
    {
        for (var i = 0; i < 10; i++)
        {
            StartCoroutine(Explode());
        }

        Defeat.SetActive(true);
        Defeat.GetComponentInChildren<Button>().onClick.AddListener(new UnityEngine.Events.UnityAction(() => { SceneManager.LoadScene("Main Menu"); }));
    }

    public void EndTurnImmediately()
    {
        SoundManager.Instance.PlaySound("belay", 7);
        StartCoroutine(EndTurnCoroutine());
    }

    private void Start()
    {
        ShipRoomDefinitions.Initialize();
        _crew = new List<MeepleBehaviour>();
        _ship = new ShipViewModel();
        _roomsOnFire = new List<ShipRoom>();
        _siegedRooms = new List<ShipRoom>();
        _ship.ConstructShip(this, RoomPrefab, ShipUIParent);
        _shieldColorChange = new Dictionary<Sector, IEnumerator>();
        _currentHullHealth = -1;
        Characters = new List<CharacterViewModel>();

        SpawnCharacter(Character.Division.Command);
        SpawnCharacter(Character.Division.Security);
        SpawnCharacter(Character.Division.Engineering);
        SpawnCharacter(Character.Division.Science);

        FindObjectOfType<CharacterPanelBehaviour>().Initialize(this);

        _jumpDeck = new JumpLocationDeck();
        _crisisDeck = new CrisisDeck(null);
    }

    private void SpawnCharacter(Character.Division division)
    {
        // Maybe convert this to not being UI?
        var crew = Instantiate(MeeplePrefab);
        crew.Initialize(this, new Character(division));
        _crew.Add(crew);
        _ship._bridge.AddCrew(crew);
        Characters.Add(crew.ViewModel);
    }

    private void Update()
    {
        if (_ship.Ship.Hull <= 0 || _ship.Ship.EnergyRemaining <= 0)
        {
            Lose();
        }

        ////if (Input.GetKeyDown(KeyCode.V))
        ////{
        ////    Win();
        ////}
        ////if (Input.GetKeyDown(KeyCode.L))
        ////{
        ////    Lose();
        ////}

        PowerRemainingText.text = string.Empty + _ship.Ship.EnergyRemaining;
        DistanceToEarthText.text = "Distance before FTL drive can reach Earth: " + _ship.Ship.DistanceToEarth + " ly";
        if (_currentHullHealth != _ship.Ship.Hull)
        {
            if (_currentHullHealth != -1)
            {
                StartCoroutine(Explode());
            }

            _currentHullHealth = _ship.Ship.Hull;
            for (var i = 0; i < HullPanel.childCount; i++)
            {
                HullPanel.GetChild(i).gameObject.SetActive(i < _currentHullHealth);
            }
        }

        for (var i = 0; i < Characters.Count; i++)
        {
            if (Characters[i].Health == 0)
            {
                Characters.RemoveAt(i--);
            }
        }

        for (var i = 0; i < ShieldSectors.Length; i++)
        {
            var active = _ship.Ship.Shields[(Sector)(i + 1)] > 0;
            var shieldImage = ShieldSectors[i].GetChild(0).GetComponent<Image>();
            var currentActive = ShieldSectors[i].GetComponent<Image>().color.a > .01f;
            ShieldSectors[i].GetComponent<Image>().color = active ? new Color(1, 1, 1) : new Color(1, 1, 1, 0);

            if (currentActive != active && !active)
            {
                shieldImage.color = new Color(.898f, 0, 0, .4f);
                if (_shieldColorChange.ContainsKey((Sector)i + 1) && _shieldColorChange[(Sector)(i + 1)] != null)
                {
                    StopCoroutine(_shieldColorChange[(Sector)(i + 1)]);
                }

                _shieldColorChange[(Sector)(i + 1)] = ChangeShieldColorCoroutine(shieldImage, new Color(shieldImage.color.r, shieldImage.color.g, shieldImage.color.b, 0), 150);
                StartCoroutine(_shieldColorChange[(Sector)(i + 1)]);
                SoundManager.Instance.PlaySound("shieldsdown", 6);
            }
            else if (currentActive != active && active)
            {
                shieldImage.color = new Color(.047f, .898f, .686f, 0);
                if (_shieldColorChange.ContainsKey((Sector)i + 1) && _shieldColorChange[(Sector)(i + 1)] != null)
                {
                    StopCoroutine(_shieldColorChange[(Sector)(i + 1)]);
                }

                _shieldColorChange[(Sector)(i + 1)] = ChangeShieldColorCoroutine(shieldImage, new Color(shieldImage.color.r, shieldImage.color.g, shieldImage.color.b, .141f), 150);
                StartCoroutine(_shieldColorChange[(Sector)(i + 1)]);
            }

            ShieldSectors[i].GetChild(1).gameObject.SetActive(_ship.Ship.Shields[(Sector)(i + 1)] > 1);
            ShieldSectors[i].GetChild(2).gameObject.SetActive(_ship.Ship.Shields[(Sector)(i + 1)] > 2);
        }

        if (!_processingTurnChange && _crew.All(x => !x.ViewModel.CanTakeAction() || InteractableRooms(x) == 0))
        {
            StartCoroutine(EndTurnCoroutine());
        }
    }

    private int InteractableRooms(MeepleBehaviour meeple)
    {
        var count = 0;

        foreach (var room in ShipUIParent.GetComponentsInChildren<ShipRoomBehaviour>())
        {
            if (room.Room.CanTakeAction(_ship, meeple.ViewModel))
            {
                count++;
            }
        }

        return count;
    }


    public GameObject ParticlePrefab;
    private IEnumerator Explode()
    {
        SoundManager.Instance.PlaySound("explosion", 1);
        var particle = Instantiate(ParticlePrefab, ShipUIParent);
        particle.transform.localPosition = UnityEngine.Random.insideUnitCircle * 100;
        particle.SetActive(true);
        foreach (var p in particle.GetComponentsInChildren<ParticleSystem>())
        {
            p.Play();
        }

        while (particle.GetComponent<ParticleSystem>().IsAlive())
        {
            yield return null;
        }

        Destroy(particle);
    }

    private IEnumerator AwayMissionCoroutine(CharacterViewModel character)
    {
        _coroutine = true;

        yield return DieRoller.RollDiceCoroutine("Social Skill Check", character.SocialSkill, 1);
        _ship.Ship.EnergyRemaining += DieRoller.SuccessCount;

        _coroutine = false;
    }

    private IEnumerator ChangeShieldColorCoroutine(Image shield, Color targetColor, float frames)
    {
        for (var i = 0; i < frames; i++)
        {
            shield.color = Color.Lerp(shield.color, targetColor, i / frames);
            yield return null;
        }
    }

    private IEnumerator WarpEffect()
    {
        Raycaster.enabled = false;
        var lines = new Dictionary<LineRenderer, Vector3>();
        WarpLines.gameObject.SetActive(true);
        SoundManager.Instance.PlaySound("enterjump", 6);
        foreach (var lr in WarpLines.GetComponentsInChildren<LineRenderer>())
        {
            var dir = ShipUIParent.transform.position + Vector3.right * 15;
            dir -= dir.z * Vector3.forward;
            lines[lr] = (lr.transform.position - dir).normalized * .01f;
            lr.SetPosition(0, lr.transform.position);
            lr.endColor = new Color(.76f, .89f, .97f, UnityEngine.Random.Range(.6f, .9f));
            lr.startColor = new Color(.87f, .89f, .97f, UnityEngine.Random.Range(.6f, .9f));
        }


        var original = ShipUIParent.transform.position;
        for (var i = 0; i < 300; i++)
        {
            foreach (var lineRenderer in new List<LineRenderer>(lines.Keys))
            {
                lineRenderer.startColor = new Color(lineRenderer.startColor.r, lineRenderer.startColor.g, lineRenderer.startColor.b + UnityEngine.Random.Range(-.3f, .3f));

                if (lines[lineRenderer].magnitude < .2f)
                {
                    lines[lineRenderer] *= 1.05f;
                }

                lineRenderer.SetPosition(1, lineRenderer.transform.position + lines[lineRenderer] * i + (Vector3)UnityEngine.Random.insideUnitCircle * .1f * i / 20f);
            }

            if (i == 150)
            {
                SoundManager.Instance.PlaySound("jump", 5);
            }

            if (i > 150 && i < 200)
            {
                ShipUIParent.transform.position = Vector3.Lerp(ShipUIParent.transform.position, ShipUIParent.transform.position + Vector3.right * .1f, (i - 150) / 50f);
                foreach (var lr in lines.Keys)
                {
                    lr.SetPosition(0, lr.transform.position + ShipUIParent.transform.position - original);
                    lr.endColor = Color.Lerp(lr.endColor, new Color(1, 1, 1, 0), (i - 150) / 50f);
                }
            }

            if (i > 150)
            {
                foreach (var ship in EnemyShipSectors.SelectMany(x => x.GetComponentsInChildren<EnemyShipBehaviour>()))
                {
                    ship.transform.position = Vector3.Lerp(ship.transform.position, ship.transform.position + Vector3.left, (i - 150) / 50f);
                }
            }

            yield return null;
        }

        for (var i = 0; i < 50; i++)
        {
            ShipUIParent.transform.position = Vector3.Lerp(ShipUIParent.transform.position, original, i / 50f);
            foreach (var lr in lines.Keys)
            {
                lr.SetPosition(0, lr.transform.position + ShipUIParent.transform.position - original);
                lr.startColor = Color.Lerp(lr.startColor, new Color(1, 1, 1, 0), i / 50f);
            }

            yield return null;
        }

        ShipUIParent.transform.position = original;

        WarpLines.gameObject.SetActive(false);
        Raycaster.enabled = true;
    }

    private IEnumerator EndTurnCoroutine()
    {
        while (_coroutine)
        {
            yield return null;
        }

        Raycaster.enabled = false;
        _processingTurnChange = true;
        yield return new WaitForSeconds(.25f);
        foreach (var crew in _crew)
        {
            crew.ViewModel.StartTurn();
        }

        foreach (var siege in _siegedRooms)
        {
            _ship.DamageRoom(siege, 1);
        }

        yield return ResolveCrisisCoroutine();

        _processingTurnChange = false;

        while (EnemyShipSectors.SelectMany(x => x.GetComponentsInChildren<EnemyShipBehaviour>()).Any(x => x.IsAttacking))
        {
            yield return null;
        }

        foreach (var fire in _roomsOnFire)
        {
            _ship.DamageRoom(fire, 1);
        }

        Raycaster.enabled = true;
        foreach (var room in ShipUIParent.GetComponentsInChildren<ShipRoomBehaviour>())
        {
            room.GetComponent<Button>().interactable = true;
        }
    }

    public void ResolveCrisis(int num = 1)
    {
        StartCoroutine(ResolveCrisisCoroutine(num));
    }

    private IEnumerator ResolveCrisisCoroutine(int num = 1)
    {
        _coroutine = true;
        for (var i = 0; i < num; i++)
        {
            var card = _crisisDeck.DrawCard();
            yield return CrisisPanel.RunCrisisCoroutine(card);
            card.PerformCrisis(this, _ship);
        }

        _coroutine = false;
    }

    public void PlotFtlJump(int peekAmt)
    {
        StartCoroutine(PlotFtlJumpCoroutine(peekAmt));
    }

    public void BeginTraining()
    {
        StartCoroutine(HolodeckTrainingCoroutine());
    }

    public void ExecuteJump(CharacterViewModel character)
    {
        StartCoroutine(ExecuteJumpCoroutine(character));
    }

    private IEnumerator HolodeckTrainingCoroutine()
    {
        _coroutine = true;
        yield return Holodeck.RunTraining(SelectedMeeple.ViewModel);
        _coroutine = false;
    }

    private IEnumerator ExecuteJumpCoroutine(CharacterViewModel character)
    {
        _coroutine = true;

        if (_ship.Ship.NextJumpLocation == null)
        {
            var planets = new List<JumpLocationBehaviour>();
            var planet = Instantiate(JumpLocationPrefab);
            planet.Initialize(_jumpDeck.DrawCard());
            planets.Add(planet);

            yield return JumpLocationSelector.WaitForSelection(planets);
            _ship.Ship.NextJumpLocation = JumpLocationSelector.LastSelection.Contents;
            Destroy(JumpLocationSelector.LastSelection.gameObject);
        }
        else
        {
            _ship._bridge.SetHeldCard(null);
        }

        if (_ship.Ship.RiskyJump())
        {
            yield return DieRoller.RollDiceCoroutine("Pilot Skill Check", character.PilotSkill, (int)Mathf.FloorToInt(_ship.Ship.NextJumpLocation.Distance / 10));
        }

        if (_ship.Ship.NextJumpLocation.Distance != 0)
        {
            yield return WarpEffect();
            ClearEnemyShips();
            CurrentSectorText.text = _ship.Ship.NextJumpLocation.Name;
        }

        SetCrisisDeck(_ship.Ship.NextJumpLocation.CrisisDeck);
        _coroutine = false;
        _ship.ExecuteJump(this, DieRoller.SuccessCount);
    }

    private IEnumerator PerformAttackCoroutine(CharacterViewModel character)
    {
        _coroutine = true;
        yield return DieRoller.RollDiceCoroutine("Combat Skill Check", character.CombatSkill, 1);

        yield return ShipSelector.WaitForTargetSelection(DieRoller.SuccessCount);

        _coroutine = false;
    }

    private IEnumerator PlotFtlJumpCoroutine(int peekAmt)
    {
        _coroutine = true;
        var planets = new List<JumpLocationBehaviour>();
        for (var i = 0; i < peekAmt; i++)
        {
            var planet = Instantiate(JumpLocationPrefab);
            planet.Initialize(_jumpDeck.DrawCard());
            planets.Add(planet);
        }

        if (_ship.Ship.DistanceToEarth <= 0)
        {
            var planet = Instantiate(JumpLocationPrefab);
            planet.Initialize(JumpLocationDeck.Earth);
            planets.Add(planet);
        }

        yield return JumpLocationSelector.WaitForSelection(planets);

        _ship.SetJumpLocation(JumpLocationSelector.LastSelection);
        _coroutine = false;
    }

    private IEnumerator ActInRoomCoroutine(ShipRoomBehaviour roomView)
    {
        var room = roomView.Room;
        var choiceIndex = 0;
        var useRoomIndex = -1;
        var mutinyIndex = -1;
        var invasionIndex = -1;
        var repairIndex = -1;

        var actions = new List<ActionChooserBehaviour.Selection>();

        if (room.CurrentDamage != 0 && !room.IsOnFire)
        {
            actions.Add(new ActionChooserBehaviour.Selection("Repair", "Perform a Repair skill check to repair damage in this room."));
            repairIndex = actions.Count - 1;
        }

        if (room.IsOnFire)
        {
            actions.Add(new ActionChooserBehaviour.Selection("Extinguish Fire", "Perform a Repair skill check to extinguish the fire in this room."));
            repairIndex = actions.Count - 1;
        }

        if (room.InRevolt)
        {
            actions.Add(new ActionChooserBehaviour.Selection("Calm Revolt", "Perform a Social skill check to calm the crew down."));
            mutinyIndex = actions.Count - 1;
        }

        if (room.Sieged)
        {
            actions.Add(new ActionChooserBehaviour.Selection("Shoot Aliens", "Perform a Combat skill check to remove invasion forces in this room."));
            invasionIndex = actions.Count - 1;
        }

        if (room.IsOperational && room.CanUse(_ship, SelectedMeeple.ViewModel))
        {
            actions.Add(new ActionChooserBehaviour.Selection(room.ActionName, room.LengthyActionDescription));
            useRoomIndex = actions.Count - 1;
        }

        if (actions.Count != 0)
        {
            yield return ActionSelector.WaitForSelection(roomView.transform.position, actions);
            choiceIndex = ActionSelector.ChoiceIndex;
        }

        if (choiceIndex == repairIndex)
        {
            if (!room.IsOnFire)
            {
                yield return DieRoller.RollDiceCoroutine("Repair Skill Check", SelectedMeeple.ViewModel.RepairSkill, 1);
                if (DieRoller.SuccessCount > 0)
                {
                    room.Repair(DieRoller.SuccessCount);
                }
            }
            else
            {
                yield return DieRoller.RollDiceCoroutine("Extinguish Fire", SelectedMeeple.ViewModel.RepairSkill, 1);
                if (DieRoller.SuccessCount > 0)
                {
                    _roomsOnFire.Remove(room);
                    room.IsOnFire = false;
                }
            }
        }
        else if (choiceIndex == mutinyIndex)
        {
            yield return DieRoller.RollDiceCoroutine("Social Skill Check", SelectedMeeple.ViewModel.SocialSkill, 1);
            if (DieRoller.SuccessCount > 0)
            {
                room.InRevolt = false;
            }
        }
        else if (choiceIndex == invasionIndex)
        {
            yield return DieRoller.RollDiceCoroutine("Combat Skill Check", SelectedMeeple.ViewModel.CombatSkill, 1);
            if (DieRoller.SuccessCount > 0)
            {
                _siegedRooms.Remove(room);
                room.Sieged = false;
            }
        }
        else if (choiceIndex == useRoomIndex)
        {
            // TODO: Multiple possible actions in a single room? Torpedos vs Phasers, Impulse vs Jump Charge?
            _ship.UseRoom(this, SelectedMeeple.ViewModel, room);
        }

        while (_coroutine)
        {
            yield return null;
        }

        SelectedMeeple.ViewModel.UseAction();

        if (!SelectedMeeple.ViewModel.CanTakeAction())
        {
            SelectedMeeple.SetSelected(false);
            SelectedMeeple = null;
            foreach (var r in ShipUIParent.GetComponentsInChildren<ShipRoomBehaviour>())
            {
                r.GetComponent<Button>().interactable = true;
            }
        }
    }
}
