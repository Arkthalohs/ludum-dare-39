﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpLocationSelectorBehaviour : MonoBehaviour
{
    public JumpLocationBehaviour LastSelection;
    private bool _selecting;

    public IEnumerator WaitForSelection(List<JumpLocationBehaviour> options)
    {
        _selecting = true;
        gameObject.SetActive(true);

        for (var i = 0; i < options.Count; i++)
        {
            var option = options[i];
            option.transform.SetParent(transform);
            option.transform.localScale = Vector3.one;
            option.Button.onClick.AddListener(new UnityEngine.Events.UnityAction(() => 
            {
                LastSelection = option;
                _selecting = false;
                SoundManager.Instance.PlaySound("blip", 4);
            }));
        }

        var startTime = Time.time;
        while (_selecting)
        {
            yield return null;
            if (options.Count == 1 && Time.time - startTime > 5f)
            {
                LastSelection = options[0];
                break;
            }
        }

        foreach (var option in options)
        {
            if (option == LastSelection)
            {
                continue;
            }

            Destroy(option.gameObject);
        }

        gameObject.SetActive(false);
    }
}
