﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ShipRoom
{
    public string Name;
    public string Description;
    public int EnergyCost;
    public int MinCharge;
    public int MaxCharge;
    public int CurrentCharge;
    public int CrewCapacity;
    public string ActionName;
    public string LengthyActionDescription;
    public Func<ShipViewModel, CharacterViewModel, bool> ActionCanBePerformed;
    public Action<ControllerBehaviour, ShipViewModel, CharacterViewModel> OnUse;

    public bool IsOnFire;
    public bool InRevolt;
    public bool Sieged;
    public int CurrentDamage;
    public List<CharacterViewModel> PresentCrew;

    public ShipRoom()
    {
        CrewCapacity = 4;
        MinCharge = EnergyCost = MaxCharge = -1;
        OnUse = (a, b, c) => { };
        ActionCanBePerformed = (ship, character) => true;
        Reset();
    }

    public void Reset()
    {
        CurrentCharge = 0;
        PresentCrew = new List<CharacterViewModel>();
    }

    public bool TracksCharge
    {
        get
        {
            return MaxCharge > 0;
        }
    }

    public bool HasCrewCapacity
    {
        get
        {
            return PresentCrew.Count < CrewCapacity;
        }
    }

    public bool IsOperational
    {
        get
        {
            return CurrentDamage < CrewCapacity;
        }
    }

    public void AddCharge()
    {
        CurrentCharge = Mathf.Min(CurrentCharge + 1, MaxCharge);
    }

    public bool CanTakeAction(ShipViewModel ship, CharacterViewModel character)
    {
        return CurrentDamage > 0 || IsOnFire || InRevolt || Sieged || CanUse(ship, character);
    }

    public bool CanUse(ShipViewModel ship, CharacterViewModel character)
    {
        return ActionCanBePerformed(ship, character) && ship.Ship.EnergyRemaining >= EnergyCost && !IsOnFire && !InRevolt && !Sieged;
    }

    public void Damage(int damageAmount)
    {
        CurrentDamage = Mathf.Min(CurrentDamage + damageAmount, CrewCapacity);
    }

    public void Repair(int repairAmt = 1)
    {
        CurrentDamage = Mathf.Max(0, CurrentDamage - repairAmt);
    }
}

