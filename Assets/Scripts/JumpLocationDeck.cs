﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class JumpLocationDeck
{
    public static JumpLocation Earth = new JumpLocation("Earth", "Home. The pale blue dot.", "earth", -1, (player, ship) => player.Win(), null);

    private List<JumpLocation> _cards;
    private List<JumpLocation> _discards;

    public JumpLocationDeck()
    {
        _cards = new List<JumpLocation>();
        _discards = new List<JumpLocation>();

        Add(_cards, JumpLocationDefinitions.Asteroids);
        Add(_cards, JumpLocationDefinitions.DischargeDrive);
        Add(_cards, JumpLocationDefinitions.NearStellarOrbit);
        Add(_cards, JumpLocationDefinitions.PirateBase);
        Add(_cards, JumpLocationDefinitions.TimeBubble);
        Add(_cards, JumpLocationDefinitions.Alien);

        Shuffle();
    }

    private void Add<T>(List<T> list, T entry, int numToAdd = 1)
    {
        for (var i = 0; i < numToAdd; i++)
        {
            list.Add(entry);
        }
    }

    public JumpLocation DrawCard()
    {
        if (_cards.Count == 0)
        {
            Shuffle();
        }

        var card = _cards[0];
        _cards.RemoveAt(0);
        _discards.Add(card);
        return card;
    }

    private void Shuffle()
    {
        var newDeck = new List<JumpLocation>(_cards.Count);
        var random = new UnityEngine.Random();
        while (_cards.Count > 0)
        {
            var idx = UnityEngine.Random.Range(0, _cards.Count);
            newDeck.Add(_cards[idx]);
            _cards.RemoveAt(idx);
        }

        while (_discards.Count > 0)
        {
            var idx = UnityEngine.Random.Range(0, _discards.Count);
            newDeck.Add(_discards[idx]);
            _discards.RemoveAt(idx);
        }

        _cards = newDeck;
    }
}
