﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JumpLocationBehaviour : MonoBehaviour
{
    // More generic?
    public JumpLocation Contents;

    public Text Title;
    public Text Description;
    public Text Distance;
    public Button Button;
    public Image Image;

    public void Initialize(JumpLocation jump)
    {
        Contents = jump;
        Title.text = jump.Name;
        Description.text = jump.Description;
        Distance.text = jump.Distance > 0 ? "Distance: " + jump.Distance : string.Empty;
        Button.onClick = new Button.ButtonClickedEvent();
        Image.sprite = Resources.Load<Sprite>(jump.PictureName);
    }
}
