﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CrisisCardBehaviour : MonoBehaviour
{
    public Text CrisisCardTitleText;
    public Text CrisisCardText;

    private bool _noClick;

    public IEnumerator FlipCoroutine(CrisisCard card)
    {
        CrisisCardTitleText.text = card.Name;
        CrisisCardText.text = card.Description;

        yield return WaitForClick();
        SoundManager.Instance.PlaySound("blip", 5);

        var flipped = Quaternion.Euler(0, 180, 0);
        float max = 50;
        for (var i = 0; i < max; i++)
        {
            transform.localRotation = Quaternion.Lerp(transform.localRotation, flipped, i / max);
            yield return null;
        }

        yield return WaitForClick();
        SoundManager.Instance.PlaySound("blip", 5);
        transform.localRotation = Quaternion.identity;
    }

    public IEnumerator WaitForClick()
    {
        _noClick = true;
        while (_noClick)
        {
            yield return null;
        }
    }

    private void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            _noClick = false;
        }
    }

    private void Start()
    {
        foreach (var button in GetComponentsInChildren<Button>())
        {
            button.onClick.AddListener(new UnityEngine.Events.UnityAction(() => { _noClick = false; }));
        }
    }
}
