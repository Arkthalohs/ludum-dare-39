﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrisisCardPanel : MonoBehaviour
{
    public CrisisCardBehaviour Card;
    public IEnumerator RunCrisisCoroutine(CrisisCard card)
    {
        var cardObj = Instantiate(Card, transform);
        cardObj.transform.localPosition = Vector3.zero;
        gameObject.SetActive(true);
        yield return cardObj.FlipCoroutine(card);
        gameObject.SetActive(false);
        Destroy(cardObj.gameObject);
    }
}
