﻿using System;
using System.Collections.Generic;
using System.Linq;

public class CrisisCard
{
    public string Name;
    public string Description;
    public Action<ControllerBehaviour, ShipViewModel> PerformCrisis;
}
