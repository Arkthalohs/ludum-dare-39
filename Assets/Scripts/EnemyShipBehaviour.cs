﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class EnemyShipBehaviour : MonoBehaviour
{
    public LineRenderer Phaser;
    private float _strayDistance = 100;
    private Vector3 _center;
    private float _angle;
    private bool _strafingRun;
    private float _speed;
    private Vector3 _fireOffset;

    private ShipRoom[] Rooms = new ShipRoom[]
    {
        ShipRoomDefinitions.Bridge,
        ShipRoomDefinitions.Shields,
        ShipRoomDefinitions.Weapons,
        ShipRoomDefinitions.Holodeck,
        ShipRoomDefinitions.Sensors,
        ShipRoomDefinitions.Medbay,
        ShipRoomDefinitions.Engineering
    };

    public bool IsAttacking
    {
        get
        {
            return _strafingRun;
        }
    }

    public Sector CurrentSector
    {
        get
        {
            var sector = Sector.None;
            if (_center.x > 0 && _center.y > 0)
            {
                sector = Sector.NorthEast;
            }
            else if (_center.x > 0 && _center.y < 0)
            {
                sector = Sector.SouthEast;
            }
            else if (_center.x < 0 && _center.y > 0)
            {
                sector = Sector.NorthWest;
            }
            else if (_center.x < 0 && _center.y < 0)
            {
                sector = Sector.SouthWest;
            }

            return sector;
        }
    }

    public void FireOn(ShipViewModel ship)
    {
        // TODO: Some sort of visual needs to be here
        var sector = CurrentSector;
        var choice = UnityEngine.Random.Range(0, 8);
        if (choice == 7)
        {
            var shipTransform = FindObjectOfType<ControllerBehaviour>().ShipUIParent;
            StartCoroutine(StrafingRunOn(shipTransform.position, ship, null, sector));
        }
        else
        {
            var roomTransform = FindObjectsOfType<ShipRoomBehaviour>().First(x => x.Room == Rooms[choice]).transform;
            StartCoroutine(StrafingRunOn(roomTransform.position, ship, Rooms[choice], sector));
        }
    }

    private IEnumerator StrafingRunOn(Vector3 point, ShipViewModel ship, ShipRoom target, Sector sector)
    {
        _strafingRun = true;

        var lerp = 25f;
        for (var i = 0; i < lerp; i++)
        {
            var targetDir = transform.position - point;
            var targetAngle = Mathf.Atan2(targetDir.y, targetDir.x) * Mathf.Rad2Deg + 180;
            _angle = Mathf.LerpAngle(_angle, targetAngle, i / lerp);
            var velocity = Quaternion.Euler(0, 0, _angle) * Vector3.right * .05f;
            transform.position += velocity;
            transform.LookAt(transform.position + velocity);
            yield return null;
        }

        while (true)
        {
            var targetDir = transform.position - point;
            var velocity = Quaternion.Euler(0, 0, _angle) * Vector3.right * _speed;
            _speed *= 1.01f;
            transform.position += velocity;

            if (targetDir.magnitude < 10)
            {
                Phaser.gameObject.SetActive(true);
                Phaser.SetPosition(1, point + _fireOffset);
                Phaser.endColor = new Color(Phaser.endColor.r, Phaser.endColor.g, Phaser.endColor.b, (targetDir.magnitude % 1.5f) - .5f);
                Phaser.startColor = new Color(Phaser.startColor.r, Phaser.startColor.g, Phaser.startColor.b, (targetDir.magnitude % 1.5f) - .5f);

                if ((targetDir.magnitude % 1.5f) < .5f)
                {
                    _fireOffset = UnityEngine.Random.insideUnitCircle;
                    SoundManager.Instance.PlaySound("shoot", 1);
                }
            }

            if (targetDir.magnitude < 1 + UnityEngine.Random.Range(0, 3))
            {
                Phaser.gameObject.SetActive(false);
                break;
            }

            yield return null;
        }

        yield return null;

        if (target != null)
        {
            ship.DamageRoom(target, 1, sector);
        }
        else
        {
            ship.DamageHull(1, sector);
        }

        _strafingRun = false;
    }

    public void Pause(bool pause)
    {
        _strafingRun = pause;
    }

    private void Start()
    {
        _center = transform.position;
        _angle = UnityEngine.Random.Range(0, 360);
    }

    private void Update()
    {
        if (!GetComponentInChildren<Image>().enabled)
        {
            var v = Quaternion.Euler(0, 0, _angle) * Vector3.right * _speed;
            transform.position += v;
            _speed *= .99f;
            return;
        }

        if (_strafingRun)
        {
            Phaser.SetPosition(0, transform.position);
            return;
        }

        if (_speed > .05f)
        {
            _speed *= .98f;
        }
        else
        {
            _speed = .05f;
        }

        var velocity = Quaternion.Euler(0, 0, _angle) * Vector3.right * _speed;
        transform.position += velocity;
        var dirToCenter = transform.position - _center;
        var angleToCenter = Mathf.Atan2(dirToCenter.y, dirToCenter.x) * Mathf.Rad2Deg + 180;

        _strayDistance = Mathf.Clamp(_strayDistance + UnityEngine.Random.Range(-10, 10), 0, 140);
        _angle = Mathf.LerpAngle(_angle, angleToCenter, dirToCenter.magnitude / _strayDistance);
        if (_strayDistance == 0)
        {
            _strayDistance = 100;
        }

        transform.LookAt(transform.position + velocity);
    }
}
