﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance;
    public List<AudioClip> Sounds;
    public AudioClip Music;

    public void PlaySound(string name, float volume)
    {
        var sound = Sounds.First(x => string.Equals(x.name.ToLower(), name));
        if (sound)
        {
            AudioSource.PlayClipAtPoint(sound, transform.position + Vector3.forward * (10 - volume));
        }
    }

    private void Start()
    {
        Instance = this;
        var audio = gameObject.AddComponent<AudioSource>();
        audio.clip = Music;
        audio.loop = true;
        audio.Play();
    }
}
