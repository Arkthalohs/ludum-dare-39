﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class CrisisDefinitions
{
    public static CrisisCard DamageRoom(ShipRoom room)
    {
        return new CrisisCard()
        {
            Name = "Explosion in " + room.Name,
            Description = "Apply 1 damage to the " + room.Name,
            PerformCrisis = (player, ship) =>
            {
                ship.DamageRoom(room, 1);
            }
        };
    }

    public static CrisisCard MutinyRoom(ShipRoom room)
    {
        return new CrisisCard()
        {
            Name = "Mutiny in " + room.Name,
            Description = "The crew in the " + room.Name + " are rioting, making it impossible to do any work there. Pass a Social skill check there to stop the mutiny",
            PerformCrisis = (player, ship) =>
            {
                player.StartMutiny(room);
            }
        };
    }

    public static CrisisCard InvadeRoom(ShipRoom room)
    {
        return new CrisisCard()
        {
            Name = "Aliens in " + room.Name,
            Description = "Aliens have been spotted in the " + room.Name + ", preventing using the room and damaging it each turn. Pass a Combat skill check to remove them.",
            PerformCrisis = (player, ship) =>
            {
                player.StartInvasion(room);
            }
        };
    }

    public static CrisisCard IgniteRoom(ShipRoom room)
    {
        return new CrisisCard()
        {
            Name = room.Name + " Wiring Short Circut",
            Description = "A fire has started in the " + room.Name + ". Every round that it is not extinguished, it will do another damage to that room",
            PerformCrisis = (player, ship) =>
            {
                player.StartFire(room);
            }
        };
    }

    public static CrisisCard FtlDischarge = new CrisisCard()
    {
        Name = "FTL Jump Core Discharge",
        Description = "Reduce the Jump Charge track by 1",
        PerformCrisis = (player, ship) =>
        {
            ship.ChargeJumpDrive(-1);
        }
    };

    public static CrisisCard Nothing = new CrisisCard()
    {
        Name = "Quiet",
        Description = "Too quiet?",
        PerformCrisis = (player, ship) => { }
    };


    public static CrisisCard DoubleTrouble = new CrisisCard()
    {
        Name = "Double Trouble",
        Description = "Immediately draw and resolve 2 additional Crisis cards",
        PerformCrisis = (player, ship) =>
        {
            player.ResolveCrisis(2);
        }
    };


    public static CrisisCard InTheVents = new CrisisCard()
    {
        Name = "It's in the Vents!",
        Description = "A random crew member takes 2 damage",
        PerformCrisis = (player, ship) =>
        {
            player.Characters[UnityEngine.Random.Range(0, player.Characters.Count)].Damage(2);
        }
    };


    public static CrisisCard RuthlessAttack = new CrisisCard()
    {
        Name = "Ruthless attack!",
        Description = "All crew members take 1 damage",
        PerformCrisis = (player, ship) =>
        {
            foreach (var c in player.Characters)
            {
                c.Damage(1);
            }
        }
    };


    public static CrisisCard ShieldDischarge(Sector sector)
    {
        return new CrisisCard()
        {
            Name = "Zzzzt...!",
            Description = "Shields in the " + sector + " quadrant have shorted out, draining all charge",
            PerformCrisis = (player, ship) =>
            {
                ship.DamageShields(sector, ship.Ship.Shields[sector]);
            }
        };
    }

    public static CrisisCard EnemySpawn(Sector sector)
    {
        if (sector == 0)
        {
            return new CrisisCard()
            {
                Name = "Unidentified Jump Signatures",
                Description = "Add 1 enemy fighter to each sector around the ship",
                PerformCrisis = (player, ship) =>
                {
                    player.SpawnEnemyInSector(1, 1);
                    player.SpawnEnemyInSector(2, 1);
                    player.SpawnEnemyInSector(3, 1);
                    player.SpawnEnemyInSector(4, 1);
                }
            };
        }
        
        return new CrisisCard()
        {
            Name = "Unidentified Jump Signatures",
            Description = "Add 2 enemy fighters to the " + sector + " of the ship",
            PerformCrisis = (player, ship) =>
            {
                player.SpawnEnemyInSector((int)sector, 2);
            }
        };
    }

    public static CrisisCard EnemyAttack(Sector sector)
    {
        if (sector == 0)
        {
            return new CrisisCard()
            {
                Name = "Enemy Weapons Charged",
                Description = "All enemies  fire on the ship",
                PerformCrisis = (player, ship) =>
                {
                    foreach (var enemy in player.EnemyShipSectors.SelectMany(x => x.GetComponentsInChildren<EnemyShipBehaviour>()))
                    {
                        enemy.FireOn(ship);
                    }
                }
            };
        }

        return new CrisisCard()
        {
            Name = "Enemy Weapons Charged",
            Description = "All enemies " + sector + " of the ship open fire",
            PerformCrisis = (player, ship) =>
            {
                foreach (var enemy in player.EnemyShipSectors[(int)sector - 1].GetComponentsInChildren<EnemyShipBehaviour>())
                {
                    enemy.FireOn(ship);
                }
            }
        };
    }
}
