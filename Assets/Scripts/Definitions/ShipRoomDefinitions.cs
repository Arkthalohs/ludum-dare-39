﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ShipRoomDefinitions
{
    public static void Initialize()
    {
        foreach (var room in Rooms)
        {
            room.Reset();
        }
    }

    public static ShipRoom Bridge = new ShipRoom()
    {
        Name = "Bridge",
        Description = "Execute FTL Jump, if the Jump Charge is at least 3. If it is below 5, then the character who activated it must succeed a piloting check with difficulty equal to the distance travelled or the ship sustains 2 hull damage",
        EnergyCost = 8,
        ActionName = "Execute FTL Jump",
        LengthyActionDescription = "FTL Jump to the next location. If the charge is yellow, a pass piloting check to avoid 2 Hull damage",
        OnUse = (player, ship, character) =>
        {
            player.ExecuteJump(character);
        },

        ActionCanBePerformed = (ship, character) =>
        {
            return ship.Ship.CanJump();
        }
    };

    ////public static ShipRoom Transporter = new ShipRoom()
    ////{
    ////    Name = "Transporter Room",
    ////    Description = "Launch an expedition mission",
    ////    EnergyCost = 1,
    ////    ActionName = "Away Mission",
    ////    LengthyActionDescription = "Begin an Away Mission at the current planet",
    ////    OnUse = (player, ship, character) =>
    ////    {
    ////        // TODO: Screen to select active characters, showing their equipment
    ////        // TODO: Something like Firefly's Aim To Misbehave deck?
    ////        player.BeginAwayMission(character);
    ////    }
    ////};

    public static ShipRoom Holodeck = new ShipRoom()
    {
        Name = "Holodeck",
        Description = "Improve Skills",
        EnergyCost = 3,
        ActionName = "Train",
        LengthyActionDescription = "Make a skill check with difficulty two less than the current skill to improve that skill (automatic if < 3)",
        OnUse = (player, ship, character) =>
        {
            player.BeginTraining();
        }
    };

    public static ShipRoom Engineering = new ShipRoom()
    {
        Name = "Engineering",
        Description = "Charge Jump Drive",
        ActionName = "Charge Jump Drive",
        LengthyActionDescription = "Increase the Jump Drive charge track by 1.",
        EnergyCost = 1,
        CrewCapacity = 1,
        OnUse = (player, ship, character) =>
        {
            ship.ChargeJumpDrive();
            SoundManager.Instance.PlaySound("enginecharge", 6);
        },

        ActionCanBePerformed = (ship, character) =>
        {
            return Engineering.CurrentCharge < Engineering.MaxCharge;
        }
    };

    public static ShipRoom Sensors = new ShipRoom()
    {
        Name = "Sensors",
        Description = "Choose the next FTL Jump location from the top 3 cards of the Jump Deck",
        ActionName = "Long Range Scan",
        LengthyActionDescription = "Choose the next FTL Jump Location from the top 3 cards of the Jump Deck.",
        EnergyCost = 3,
        CrewCapacity = 1,
        OnUse = (player, ship, character) =>
        {
            player.PlotFtlJump(3);
        }
    };

    public static ShipRoom Medbay = new ShipRoom()
    {
        Name = "Medbay",
        Description = "Heal by 1 HP",
        ActionName = "R&R",
        LengthyActionDescription = "Heal the current character's Health Points by 1",
        EnergyCost = 2,
        CrewCapacity = 3,
        OnUse = (player, ship, character) =>
        {
            character.Heal(1);
        },

        ActionCanBePerformed = (ship, character) =>
        {
            return character.CanHeal();
        }
    };

    public static ShipRoom Shields = new ShipRoom()
    {
        Name = "Shield Control",
        Description = "Recharge Shields to full",
        ActionName = "Decapacitate Shields",
        LengthyActionDescription = "Recharge shield percentage to full",
        EnergyCost = 15,
        CrewCapacity = 2,
        OnUse = (player, ship, character) =>
        {
            ship.Ship.Shields[Sector.NorthEast] = ship.Ship.MaxShields;
            ship.Ship.Shields[Sector.NorthWest] = ship.Ship.MaxShields;
            ship.Ship.Shields[Sector.SouthEast] = ship.Ship.MaxShields;
            ship.Ship.Shields[Sector.SouthWest] = ship.Ship.MaxShields;
            SoundManager.Instance.PlaySound("shieldcharge", 10);
        },
        ActionCanBePerformed = (ship, character) =>
        {
            return ship.Ship.Shields[Sector.NorthEast] < ship.Ship.MaxShields ||
                ship.Ship.Shields[Sector.NorthWest] < ship.Ship.MaxShields ||
                ship.Ship.Shields[Sector.SouthEast] < ship.Ship.MaxShields ||
                ship.Ship.Shields[Sector.SouthWest] < ship.Ship.MaxShields;
        }
    };

    public static ShipRoom Weapons = new ShipRoom()
    {
        Name = "Weapons",
        Description = "Deal 1 damage to an enemy vessel",
        ActionName = "Fire Phasers",
        LengthyActionDescription = "Destroy 1 enemy vessel in target sector for each success in a Combat skill check",
        EnergyCost = 3,
        CrewCapacity = 2,
        OnUse = (player, ship, character) =>
        {
            player.StartAttack(character);
        },
        ActionCanBePerformed = (ship, character) =>
        {
            return GameObject.FindObjectsOfType<EnemyShipBehaviour>().Length > 0;
        }
    };


    public static ShipRoom[] Rooms = new ShipRoom[]
    {
        ShipRoomDefinitions.Bridge,
        ShipRoomDefinitions.Shields,
        ShipRoomDefinitions.Weapons,
        ShipRoomDefinitions.Holodeck,
        ShipRoomDefinitions.Sensors,
        ShipRoomDefinitions.Medbay,
        ShipRoomDefinitions.Engineering
    };
}
