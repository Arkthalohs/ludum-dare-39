﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public class JumpLocationDefinitions
{
    public static JumpLocation Asteroids = new JumpLocation(
            "Asteroids",
            "Take damage to 3 random rooms upon arrival",
            "asteroids",
            20,
            (player, ship) =>
            {
                for (var i = 0; i < 3; i++)
                {
                    var room = ShipRoomDefinitions.Rooms[UnityEngine.Random.Range(0, ShipRoomDefinitions.Rooms.Length)];
                    ship.DamageRoom(room, 1);
                }
            },
            new CrisisDeck(null));

    public static JumpLocation NearStellarOrbit = new JumpLocation(
        "Near Stellar Orbit",
        "It's close enough to power the solar panels for a few seconds. Gain 8 power on arrival.\nHigh chance of fires.",
        "star",
        40,
        (player, ship) =>
        {
            ship.Ship.EnergyRemaining += 8;
        },
        new CrisisDeck(
            () =>
            {
                var cards = new List<CrisisCard>();
                for (var i = 0; i < ShipRoomDefinitions.Rooms.Length; i++)
                {
                    Add(cards, CrisisDefinitions.IgniteRoom(ShipRoomDefinitions.Rooms[i]), 2);
                    Add(cards, CrisisDefinitions.DamageRoom(ShipRoomDefinitions.Rooms[i]));
                    Add(cards, CrisisDefinitions.MutinyRoom(ShipRoomDefinitions.Rooms[i]));
                }

                for (var i = Sector.NorthEast; i <= Sector.SouthWest; i++)
                {
                    Add(cards, CrisisDefinitions.EnemySpawn(i));
                    Add(cards, CrisisDefinitions.EnemyAttack(i));
                    Add(cards, CrisisDefinitions.ShieldDischarge(i), 3);
                }

                Add(cards, CrisisDefinitions.FtlDischarge);
                return cards;
            }
        )
        );

    public static JumpLocation PirateBase = new JumpLocation(
        "Pirate Base",
        "All sectors immediately gain 1 enemy ship.\nHigh chance of enemy fighter attacks and invading forces.",
        "pirates",
        35,
        (player, ship) =>
        {
            player.SpawnEnemyInSector(1, 1);
            player.SpawnEnemyInSector(2, 1);
            player.SpawnEnemyInSector(3, 1);
            player.SpawnEnemyInSector(4, 1);
        },
        new CrisisDeck(() =>
        {
            var cards = new List<CrisisCard>();
            Add(cards, CrisisDefinitions.EnemyAttack(Sector.None), 3);
            Add(cards, CrisisDefinitions.EnemySpawn(Sector.None), 2);

            for (var i = Sector.NorthEast; i <= Sector.SouthWest; i++)
            {
                Add(cards, CrisisDefinitions.EnemySpawn(i), 2);
                Add(cards, CrisisDefinitions.EnemyAttack(i), 2);
                Add(cards, CrisisDefinitions.ShieldDischarge(i), 1);
            }

            for (var i = 0; i < ShipRoomDefinitions.Rooms.Length; i++)
            {
                Add(cards, CrisisDefinitions.InvadeRoom(ShipRoomDefinitions.Rooms[i]), UnityEngine.Random.Range(0, 3));
            }

            Add(cards, CrisisDefinitions.FtlDischarge, 3);

            return cards;
        })
        );

    public static JumpLocation TimeBubble = new JumpLocation(
        "Time Bubble",
        "Draw and resolve a Crisis Card immediately upon arrival.",
        "time",
        15,
        (player, ship) =>
        {
            player.ResolveCrisis();
        },
        new CrisisDeck(() =>
        {
            var cards = new List<CrisisCard>();
            Add(cards, CrisisDefinitions.DoubleTrouble, 5);
            Add(cards, CrisisDefinitions.FtlDischarge, 6);
            Add(cards, CrisisDefinitions.Nothing, 5);
            Add(cards, CrisisDefinitions.DamageRoom(ShipRoomDefinitions.Bridge), 2);
            Add(cards, CrisisDefinitions.DamageRoom(ShipRoomDefinitions.Shields), 1);
            Add(cards, CrisisDefinitions.IgniteRoom(ShipRoomDefinitions.Weapons), 1);
            Add(cards, CrisisDefinitions.DamageRoom(ShipRoomDefinitions.Weapons), 1);
            Add(cards, CrisisDefinitions.DamageRoom(ShipRoomDefinitions.Holodeck), 1);
            Add(cards, CrisisDefinitions.IgniteRoom(ShipRoomDefinitions.Sensors), 1);
            Add(cards, CrisisDefinitions.DamageRoom(ShipRoomDefinitions.Sensors), 1);
            Add(cards, CrisisDefinitions.DamageRoom(ShipRoomDefinitions.Medbay), 1);
            Add(cards, CrisisDefinitions.IgniteRoom(ShipRoomDefinitions.Engineering), 2);
            Add(cards, CrisisDefinitions.DamageRoom(ShipRoomDefinitions.Engineering), 3);

            for (var i = 0; i < ShipRoomDefinitions.Rooms.Length; i++)
            {
                Add(cards, CrisisDefinitions.InvadeRoom(ShipRoomDefinitions.Rooms[i]), UnityEngine.Random.Range(0, 2));
                Add(cards, CrisisDefinitions.MutinyRoom(ShipRoomDefinitions.Rooms[i]), UnityEngine.Random.Range(0, 2));
            }

            for (var i = Sector.NorthEast; i <= Sector.SouthWest; i++)
            {
                Add(cards, CrisisDefinitions.ShieldDischarge(i), 4);
            }

            Add(cards, CrisisDefinitions.EnemySpawn(Sector.NorthEast));
            Add(cards, CrisisDefinitions.EnemySpawn(Sector.NorthWest), 2);
            Add(cards, CrisisDefinitions.EnemySpawn(Sector.SouthEast), 3);
            Add(cards, CrisisDefinitions.EnemySpawn(Sector.SouthWest), 1);
            Add(cards, CrisisDefinitions.EnemySpawn(Sector.None), 1);
            Add(cards, CrisisDefinitions.EnemyAttack(Sector.None), 4);
            return cards;
        })
        );

    public static JumpLocation DischargeDrive = new JumpLocation(
        "Discharge Jump Drive",
        "Instead of moving, gain 30 power (max 99)",
        "power",
        0,
        (player, ship) =>
        {
            ship.Ship.EnergyRemaining = Math.Min(ship.Ship.EnergyRemaining + 30, 99);
        },
        null
        );

    public static JumpLocation Alien = new JumpLocation(
        "LV-296",
        "High risk of alien contaminant",
        "lv",
        30,
        (player, ship) =>
        {
        },
        new CrisisDeck(() =>
        {
            var cards = new List<CrisisCard>();
            Add(cards, CrisisDefinitions.DoubleTrouble, 3);
            Add(cards, CrisisDefinitions.FtlDischarge, 3);
            Add(cards, CrisisDefinitions.InTheVents, 6);
            Add(cards, CrisisDefinitions.RuthlessAttack, 2);
            Add(cards, CrisisDefinitions.DamageRoom(ShipRoomDefinitions.Bridge), 1);
            Add(cards, CrisisDefinitions.DamageRoom(ShipRoomDefinitions.Shields), 1);
            Add(cards, CrisisDefinitions.DamageRoom(ShipRoomDefinitions.Weapons), 2);
            Add(cards, CrisisDefinitions.DamageRoom(ShipRoomDefinitions.Holodeck), 1);
            Add(cards, CrisisDefinitions.DamageRoom(ShipRoomDefinitions.Sensors), 1);
            Add(cards, CrisisDefinitions.DamageRoom(ShipRoomDefinitions.Medbay), 1);
            Add(cards, CrisisDefinitions.DamageRoom(ShipRoomDefinitions.Engineering), 1);

            Add(cards, CrisisDefinitions.InvadeRoom(ShipRoomDefinitions.Holodeck), 4);
            Add(cards, CrisisDefinitions.InvadeRoom(ShipRoomDefinitions.Sensors), 3);
            Add(cards, CrisisDefinitions.InvadeRoom(ShipRoomDefinitions.Medbay),3);
            Add(cards, CrisisDefinitions.InvadeRoom(ShipRoomDefinitions.Engineering), 1);

            for (var i = Sector.NorthEast; i <= Sector.SouthWest; i++)
            {
                Add(cards, CrisisDefinitions.ShieldDischarge(i), 1);
            }

            return cards;
        })
        );



    private static void Add<T>(List<T> list, T entry, int numToAdd = 1)
    {
        for (var i = 0; i < numToAdd; i++)
        {
            list.Add(entry);
        }
    }
}