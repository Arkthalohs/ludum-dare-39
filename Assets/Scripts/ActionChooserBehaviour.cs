﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionChooserBehaviour : MonoBehaviour
{
    public Transform ActionPanel;
    public LineRenderer Line;
    public Transform ConnectionKnob;
    public int ChoiceIndex;

    private bool _noSelection;

    public IEnumerator WaitForSelection(Vector3 source, List<Selection> options)
    {
        StartSelection(options);
        Line.gameObject.SetActive(true);
        Line.SetPositions(new Vector3[] { source - Vector3.forward, ConnectionKnob.transform.position - Vector3.forward });
        while (_noSelection)
        {
            yield return null;
        }

        Line.gameObject.SetActive(false);
        gameObject.SetActive(false);
        for (var i = 1; i < ActionPanel.childCount; i++)
        {
            Destroy(ActionPanel.GetChild(i).gameObject);
        }
    }

    public void ButtonClicked(int i)
    {
        ChoiceIndex = i;
        _noSelection = false;
        SoundManager.Instance.PlaySound("blip3", 7);
    }

    private void StartSelection(List<Selection> options)
    {
        gameObject.SetActive(true);
        _noSelection = true;

        var buttonSource = ActionPanel.GetChild(0).GetComponentInChildren<Button>();
        buttonSource.onClick.RemoveAllListeners();
        for (var i = 1; i < options.Count; i++)
        {
            var newButton = Instantiate(buttonSource.transform.parent, buttonSource.transform.parent.parent).GetComponentInChildren<Button>();
            newButton.onClick.RemoveAllListeners();
        }

        for (var i = 0; i < options.Count; i++)
        {
            var option = ActionPanel.GetChild(i);
            var button = option.GetComponentInChildren<Button>();
            button.GetComponentInChildren<Text>().text = options[i].ButtonName;
            option.GetComponentsInChildren<Text>()[1].text = options[i].ExtraDescription;
            var index = i;
            button.onClick.AddListener(new UnityEngine.Events.UnityAction(() => ButtonClicked(index)));
        }
    }

    public class Selection
    {
        public string ButtonName;
        public string ExtraDescription;

        public Selection(string name, string description)
        {
            ButtonName = name;
            ExtraDescription = description;
        }
    }
}
