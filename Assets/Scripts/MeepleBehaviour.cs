﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MeepleBehaviour : MonoBehaviour
{
    public Image Meeple;
    public Outline Outline;
    public CharacterViewModel ViewModel;

    public void Initialize(ControllerBehaviour controller, Character character)
    {
        Outline.effectColor = Color.black;
        Outline.enabled = false;
        ViewModel = new CharacterViewModel(character);

        var button = GetComponent<Button>();
        button.onClick = new Button.ButtonClickedEvent();
        button.onClick.AddListener(new UnityEngine.Events.UnityAction(() =>
        {
            controller.SelectMeeple(this);
        }));

        GetComponentInChildren<Text>().text = ViewModel.Initials;
    }

    public void Update()
    {
        Meeple.color = ViewModel.GetColor();
        transform.localPosition = Vector3.zero;

        if (ViewModel.Health == 0)
        {
            Destroy(gameObject);
        }
    }

    public void SetSelected(bool isSelected)
    {
        Outline.enabled = isSelected;
    }
}
