﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class CharacterViewModel
{
    public int Actions = 1;

    private Character _character;
    private ShipRoom _currentRoom;

    public CharacterViewModel(Character character)
    {
        _character = character;
    }

    public int CombatSkill
    {
        get
        {
            return _character.CombatSkill;
        }

        set
        {
            _character.CombatSkill = value;
        }
    }

    public int RepairSkill
    {
        get
        {
            return _character.RepairSkill;
        }

        set
        {
            _character.RepairSkill = value;
        }
    }

    public int PilotSkill
    {
        get
        {
            return _character.PilotSkill;
        }

        set
        {
            _character.PilotSkill = value;
        }
    }

    public int SocialSkill
    {
        get
        {
            return _character.SocialSkill;
        }

        set
        {
            _character.SocialSkill = value;
        }
    }

    public string Name

    {
        get
        {
            return _character.Name;
        }
    }

    public int Health
    {
        get
        {
            return _character.Health;
        }
    }

    public string Initials
    {
        get
        {
            var parts = _character.Name.Split(' ');
            return string.Empty + parts[0][0] + parts[1][0];
        }
    }

    public void Damage(int amt)
    {
        _character.Health = Mathf.Max(_character.Health - amt, 0);
    }

    public void StartTurn()
    {
        Actions = 1;
    }

    public void UseAction()
    {
        Actions--;
    }

    public bool CanTakeAction()
    {
        return Actions > 0;
    }

    public void Heal(int healAmt)
    {
        _character.Health = Mathf.Min(_character.Health + healAmt, _character.MaxHealth);
    }

    public bool CanHeal()
    {
        return _character.Health < _character.MaxHealth;
    }

    public Color GetColor()
    {
        var color = Color.white;
        switch (_character.Class)
        {
            case Character.Division.Command:
                color = new Color(.1f, .7f, 0);
                break;
            case Character.Division.Engineering:
                color = new Color(.6f, .7f, 0);
                break;
            case Character.Division.Science:
                color = new Color(0, .1f, .7f);
                break;
            case Character.Division.Security:
                color = new Color(.8f, 0, 0);
                break;
        }

        if (!CanTakeAction())
        {
            return color * .5f;
        }

        return color;
    }

    public bool IsInRoom(ShipRoomBehaviour newRoom)
    {
        return _currentRoom == newRoom.Room;
    }

    public void MoveToRoom(ShipRoom newRoom)
    {
        if (_currentRoom != null)
        {
            _currentRoom.PresentCrew.Remove(this);
        }

        _currentRoom = newRoom;
        newRoom.PresentCrew.Add(this);
    }
}
