﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ShipModel
{
    public int EnergyRemaining = 99;
    public int Hull = 10;
    public JumpLocation NextJumpLocation;
    public int JumpCharge;
    public int MinJumpCharge = 3;
    public int MaxJumpCharge = 5;
    public Dictionary<Sector, int> Shields;

    public int MaxShields = 2;

    private int _distanceToEarth = 80;

    public ShipModel()
    {
        Shields = new Dictionary<Sector, int>();
        Shields[Sector.None] = 0;
        Shields[Sector.NorthEast] = MaxShields;
        Shields[Sector.NorthWest] = MaxShields;
        Shields[Sector.SouthEast] = MaxShields;
        Shields[Sector.SouthWest] = MaxShields;
    }

    public int DistanceToEarth
    {
        get
        {
            return _distanceToEarth;
        }

        set
        {
            _distanceToEarth = Mathf.Max(0, value);
        }
    }

    public bool CanJump()
    {
        return JumpCharge >= MinJumpCharge;
    }

    public bool RiskyJump()
    {
        return JumpCharge != MaxJumpCharge;
    }
}
