﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DieRoller : MonoBehaviour
{
    public DieBehaviour DiePrefab;
    public GameObject BackingPanel;
    public Text UIElement;
    public Text TitleText;
    public int SuccessCount;

    public IEnumerator RollDiceCoroutine(string title, int dieCount, int requiredSuccesses)
    {
        if (requiredSuccesses <= 0)
        {
            // Instant success;
            SuccessCount = 0;
            yield break;
        }

        BackingPanel.SetActive(true);
        UIElement.transform.parent.gameObject.SetActive(false);
        TitleText.text = title + "\n" + requiredSuccesses + " Successes Needed";
        var dice = new List<DieBehaviour>();
        SuccessCount = 0;
        for (var i = 0; i < dieCount; i++)
        {
            var die = Instantiate(DiePrefab, transform);
            die.transform.localPosition = Vector3.zero;
            dice.Add(die);
            die.Roll();
        }

        while (!dice.TrueForAll(x => x.Stopped))
        {
            yield return null;
        }

        foreach (var die in dice)
        {
            if (die.Value >= 5)
            {
                SuccessCount++;
            }
        }

        UIElement.transform.parent.gameObject.SetActive(true);
        UIElement.text = SuccessCount + " Successes\n" + (SuccessCount >= requiredSuccesses ? "Action succeeds!": "Action fails.") + "\n(Click to Continue)";
        UIElement.transform.parent.GetComponent<Image>().color = Color.white;
        UIElement.transform.parent.GetComponent<Image>().CrossFadeColor(SuccessCount >= requiredSuccesses ? new Color(.8f, 1, .8f) : new Color(1, .8f, .8f), .25f, false, false);
        yield return WaitForClick();
        SoundManager.Instance.PlaySound("blip3", 7);
        yield return new WaitForSeconds(.1f);

        BackingPanel.SetActive(false);
        UIElement.transform.parent.gameObject.SetActive(false);

        foreach (var die in dice)
        {
            Destroy(die.gameObject);
        }
    }

    public IEnumerator WaitForClick()
    {
        while (!Input.GetMouseButtonUp(0))
        {
            yield return null;
        }
    }
}
