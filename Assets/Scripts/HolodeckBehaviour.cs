﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HolodeckBehaviour : MonoBehaviour
{
    public Character.Division SelectedSkill;

    public DieRoller Roller;
    public GameObject SkillSelect;

    private bool _selected;

    public void Select(int selected)
    {
        SelectedSkill = (Character.Division)selected;
        _selected = true;
        SoundManager.Instance.PlaySound("blip2", 4);
    }

    public IEnumerator RunTraining(CharacterViewModel character)
    {
        gameObject.SetActive(true);
        SkillSelect.SetActive(true);
        _selected = false;

        while (!_selected)
        {
            yield return null;
        }
        
        switch (SelectedSkill)
        {
            case Character.Division.Command:
                yield return Roller.RollDiceCoroutine("Training Social", character.SocialSkill, character.SocialSkill - 2);
                if (Roller.SuccessCount >= character.SocialSkill - 2)
                {
                    character.SocialSkill++;
                }

                break;
            case Character.Division.Engineering:
                yield return Roller.RollDiceCoroutine("Training Repair", character.RepairSkill, character.RepairSkill - 2);
                if (Roller.SuccessCount >= character.RepairSkill - 2)
                {
                    character.RepairSkill++;
                }

                break;
            case Character.Division.Science:
                yield return Roller.RollDiceCoroutine("Training Piloting", character.PilotSkill, character.PilotSkill - 2);
                if (Roller.SuccessCount >= character.PilotSkill - 2)
                {
                    character.PilotSkill++;
                }

                break;
            case Character.Division.Security:
                yield return Roller.RollDiceCoroutine("Training Combat", character.CombatSkill, character.CombatSkill - 2);
                if (Roller.SuccessCount >= character.CombatSkill - 2)
                {
                    character.CombatSkill++;
                }
                
                break;
        }

        SkillSelect.SetActive(false);
        gameObject.SetActive(false);
    }
}
