﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipRoomBehaviour : MonoBehaviour
{
    public Text RoomTitle;
    public Text RoomDescription;
    public Text PowerCost;
    public Button Button;
    public Transform CardHolder;
    public Transform CrewHolder;
    public Transform ChargeTrack;
    public Transform ChargeTracker;
    public Image FireToken;
    public Image MutinyToken;
    public Image BoarderToken;
    public GameObject ParticlePrefab;

    public ShipRoom Room;

    // THis is terrible.
    private int _currentCharge = -1;
    private int _currentCrewCount = 0;
    private int _currentDamage = -1;

    public void Initialize(ControllerBehaviour controller, ShipRoom room)
    {
        name = RoomTitle.text = room.Name;
        RoomDescription.text = room.Description;
        Room = room;

        if (room.EnergyCost >= 0)
        {
            PowerCost.text = string.Empty + room.EnergyCost;
        }
        else
        {
            PowerCost.transform.parent.gameObject.SetActive(false);
        }

        ChargeTrack.gameObject.SetActive(room.TracksCharge);
        if (room.TracksCharge)
        {
            var source = ChargeTrack.GetChild(0);
            source.GetComponent<Image>().color = new Color(.5f, 0, 0);
            for (var i = 1; i <= room.MaxCharge; i++)
            {
                var trackEntry = Instantiate(source, ChargeTrack);
                trackEntry.GetComponentInChildren<Text>().text = string.Empty + i;
                if (i < Room.MaxCharge)
                {
                    trackEntry.GetComponent<Image>().color = i < Room.MinCharge ? new Color(.7f, 0, 0) : Color.yellow * .8f;
                }
                else
                {
                    trackEntry.GetComponent<Image>().color = Color.green * .8f;
                }
            }
        }

        var crewBack = CrewHolder.GetChild(0);
        for (var i = 1; i < room.CrewCapacity; i++)
        {
            var image = Instantiate(crewBack, CrewHolder);
        }

        Button.onClick = new Button.ButtonClickedEvent();
        Button.onClick.AddListener(new UnityEngine.Events.UnityAction(() => controller.MoveMeepleAndPerformAction(this)));
    }

    public void SetHeldCard(Transform card)
    {
        for (var i = 0; i < CardHolder.childCount; i++)
        {
            Destroy(CardHolder.GetChild(i).gameObject);
        }

        if (card)
        {
            card.SetParent(CardHolder);
            card.localScale = Vector3.one;
        }
    }

    public void AddCrew(MeepleBehaviour meeple)
    {
        if (meeple && !meeple.transform.IsChildOf(CrewHolder.transform))
        {
            meeple.transform.SetParent(CrewHolder.transform.GetChild(Room.PresentCrew.Count));
            meeple.transform.localScale = Vector3.one;
            meeple.transform.localPosition = Vector3.zero;
            meeple.ViewModel.MoveToRoom(Room);
        }
    }

    private void Update()
    {
        if (Room.TracksCharge && _currentCharge != Room.CurrentCharge)
        {
            _currentCharge = Room.CurrentCharge;
            ChargeTracker.SetParent(ChargeTrack.GetChild(_currentCharge));
            ChargeTracker.localPosition = Vector3.zero;
        }

        if (_currentCrewCount != Room.PresentCrew.Count)
        {
            _currentCrewCount = Room.PresentCrew.Count;
            var crewMembers = CrewHolder.GetComponentsInChildren<MeepleBehaviour>();
            for (var i = 0; i < crewMembers.Length; i++)
            {
                crewMembers[i].transform.SetParent(CrewHolder.transform.GetChild(i));
                crewMembers[i].transform.localPosition = Vector3.zero;
            }
        }

        if (_currentDamage != Room.CurrentDamage)
        {
            if (_currentDamage != -1 && _currentDamage < Room.CurrentDamage)
            {
                StartCoroutine(Explode());
            }

            _currentDamage = Room.CurrentDamage;
            for (var i = 0; i < CrewHolder.childCount; i++)
            {
                CrewHolder.GetChild(i).GetComponentInChildren<DamageFlashBehaviour>(true).gameObject.SetActive(i < _currentDamage);
            }
        }

        FireToken.enabled = Room.IsOnFire;
        MutinyToken.enabled = Room.InRevolt;
        BoarderToken.enabled = Room.Sieged;
    }

    private IEnumerator Explode()
    {
        SoundManager.Instance.PlaySound("explosion", 1);
        var particle = Instantiate(ParticlePrefab, transform);
        particle.transform.localPosition = UnityEngine.Random.insideUnitCircle * 50;
        particle.SetActive(true);
        foreach (var p in particle.GetComponentsInChildren<ParticleSystem>())
        {
            p.Play();
        }

        while (particle.GetComponent<ParticleSystem>().IsAlive())
        {
            yield return null;
        }

        Destroy(particle);
    }
}
