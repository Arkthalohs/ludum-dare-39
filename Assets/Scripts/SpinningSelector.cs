﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinningSelector : MonoBehaviour {

    float _rotation;

	// Update is called once per frame
	void Update () {
        transform.localRotation = Quaternion.Euler(0, 0, _rotation);
        _rotation += 5;
	}
}
